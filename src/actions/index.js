const numberButtonClick = (name) => {
	return {
		type: 'BUTTON_NUMBER',
		payload: name
	}
};

const clearButtonClick = () => {
	return {
		type: 'BUTTON_CLEAR'
	}
};

const pointButtonClick = () => {
	return {
		type: 'BUTTON_POINT'
	}
};

const plusButtonClick = () => {
	return {
		type: 'BUTTON_PLUS'
	}
};

const minusButtonClick = () => {
	return {
		type: 'BUTTON_MINUS'
	}
};

const divButtonClick = () => {
	return {
		type: 'BUTTON_DIV'
	}
};

const mulButtonClick = () => {
	return {
		type: 'BUTTON_MUL'
	}
};

const equalButtonClick = () => {
	return {
		type: 'BUTTON_EQUAL'
	}
};

export {
	numberButtonClick,
	clearButtonClick,
	pointButtonClick,
	plusButtonClick,
	equalButtonClick,
	minusButtonClick,
	divButtonClick,
	mulButtonClick,
}
