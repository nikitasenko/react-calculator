import React from 'react';
import DisplayPanel from '../display-panel';
import ButtonPanel from "../button-panel";

import './app.css';
const App = () => {
	return (
		<main role={'main'} className={'container col-4 main'}>
			<DisplayPanel/>
			<ButtonPanel/>
		</main>
	)
};

export default App;
