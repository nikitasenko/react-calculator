import React, {Component} from 'react';

import './button-panel.css';
import Button from "../button";

class ButtonPanel extends Component {
	render() {
		return (
			<div className="component-button-panel">
				<div>
					<Button name="AC" operationType={'CLEAR'}/>
					<Button name="÷" operationType={'DIV'}/>  
				</div>
				<div>
					<Button name="7" operationType={'NUMBER'}/>
					<Button name="8" operationType={'NUMBER'}/>
					<Button name="9" operationType={'NUMBER'}/>
					<Button name="x" operationType={'MUL'}/>
				</div>
				<div>
					<Button name="4" operationType={'NUMBER'}/>
					<Button name="5" operationType={'NUMBER'}/>
					<Button name="6" operationType={'NUMBER'}/>
					<Button name="-" operationType={'MINUS'}/>
				</div>
				<div>
					<Button name="1" operationType={'NUMBER'}/>
					<Button name="2" operationType={'NUMBER'}/>
					<Button name="3" operationType={'NUMBER'}/>
					<Button name="+" operationType={'PLUS'}/>
				</div>
				<div>
					<Button name="0" operationType={'NUMBER'}/>
					<Button name="." operationType={'POINT'}/>
					<Button name="=" operationType={'EQUAL'}/>
				</div>
			</div>
		)
	}
}

export default ButtonPanel;
