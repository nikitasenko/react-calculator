import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    numberButtonClick,
    clearButtonClick,
    pointButtonClick,
    plusButtonClick,
    equalButtonClick,
    minusButtonClick,
    divButtonClick,
    mulButtonClick,
} from '../../actions'
import './button.css';

const Button = ({name, handelClick, operationType}) => {
    return (
        <div className={'component-button'}>
            <button type="button" className="btn btn-outline-secondary"
                    onClick={() => handelClick(name, operationType)}
            >
                {name}
            </button>
        </div>
    )
};

Button.propTypes = {
    name: PropTypes.string,
    handelClick: PropTypes.func,
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        handelClick: (name, operationType) => {
            if (operationType === 'NUMBER') {
                return numberButtonClick(name);
            }
            if (operationType === 'CLEAR') {
                return clearButtonClick();
            }
            if (operationType === 'POINT') {
                return pointButtonClick();
            }
            if (operationType === 'PLUS') {
                return plusButtonClick();
            }
            if (operationType === 'MUL') {
                return mulButtonClick();
            }
            if (operationType === 'MINUS') {
                return minusButtonClick();
            }
            if (operationType === 'DIV') {
                return divButtonClick();
            }
            if (operationType === 'EQUAL') {
                return equalButtonClick();
            }
        }
    }, dispatch);
};


export default connect(undefined, mapDispatchToProps)(Button);
