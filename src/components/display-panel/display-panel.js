import React from 'react';
import {connect} from 'react-redux';

import './display-panel.css';

const DisplayPanel = ({value}) => {
	return (
		<div className="component-display">
			<div>{value}</div>
		</div>
	)
};
const mapStateToProps = ({displayValue}) => {
	return {
		value: displayValue,
	};
};

export default connect(mapStateToProps)(DisplayPanel);
