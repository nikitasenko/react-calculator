const initialState = {
	operation: null,
	displayValue: '0',
	prevValue: null,
};

const calculateDisplayValue = (displayValue, number) => {
	if (
		displayValue === '+' || displayValue === 'x' ||
		displayValue === '-' || displayValue === '÷'
	) {
		return number;
	}
	return displayValue === '0' ? number : displayValue + number;
};

const calculateResult = (displayValue, prevValue, operation) => {
	if (operation === 'PLUS') {
		return parseFloat(displayValue) + parseFloat(prevValue)
	}
	if (operation === 'MINUS') {
		return parseFloat(prevValue) - parseFloat(displayValue)
	}
	if (operation === 'MUL') {
		return parseFloat(displayValue) * parseFloat(prevValue)
	}
	if (operation === 'DIV') {
		return parseFloat(displayValue) / parseFloat(prevValue)
	}
	return '0';
};

const reducer = (state = initialState, action) => {
	const {displayValue, operation, prevValue} = state;
	switch (action.type) {
		case 'BUTTON_NUMBER':
			const number = action.payload;
			return {
				...state,
				displayValue: calculateDisplayValue(displayValue, number),
			};
		case 'BUTTON_CLEAR':
			return initialState;
		case 'BUTTON_POINT':
			if (displayValue.includes('.')) {
				return {
					...state,
				}
			}
			return {
				...state,
				displayValue: displayValue + '.',
			};
		case 'BUTTON_PLUS':
			return {
				prevValue: displayValue,
				operation: 'PLUS',
				displayValue: '+',
			};
		case 'BUTTON_MINUS':
			return {
				prevValue: displayValue,
				operation: 'MINUS',
				displayValue: '-',
			};
		case 'BUTTON_DIV':
			return {
				prevValue: displayValue,
				operation: 'DIV',
				displayValue: '÷',
			};
		case 'BUTTON_MUL':
			return {
				prevValue: displayValue,
				operation: 'MUL',
				displayValue: 'x',
			};
		case 'BUTTON_EQUAL':
			return {
				prevValue: displayValue,
				operation: null,
				displayValue: calculateResult(displayValue, prevValue, operation)
			};
		default:
			return state;
	}
};

export default reducer;
